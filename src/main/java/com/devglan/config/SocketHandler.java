package com.devglan.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class SocketHandler extends TextWebSocketHandler {

    static List<WebSocketSession> sessions;
    static HashMap<String, Set<String>> tourToClient;



   static {
        sessions = new ArrayList<>();
        tourToClient = new HashMap<>();
    }

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {
		List<String> tours = new ObjectMapper().readValue(message.getPayload(), List.class);

		subscribeClientToTours(session.getId(), tours);
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) {
		addToSessions(session);
	}

    public void addToSessions(WebSocketSession webSocketSession) {
        sessions.add(webSocketSession);
    }

    static public WebSocketSession getSessionsById(String sessionId) {
        for(WebSocketSession session : sessions) {
            if (session.getId() == sessionId)
                return session;
        }
        return null;
    }



    public void subscribeClientToTours(String clientId, List<String> tours) {
       for (String tour: tours) {
            Set<String> clients =  tourToClient.get(tour);
            if (clients == null || clients.isEmpty()) {
                tourToClient.put(tour, Collections.singleton(clientId));
            }
            else {
                clients.add(clientId);
                tourToClient.put(tour, clients);
            }
        }
    }
    public Map<String, Set<String>> getTourAndClients() {
        return tourToClient;
    }


    @Scheduled(fixedRate = 5000)
    public void sendMessageToClient(){
        Set<Map.Entry<String, Set<String>>> toursMap = getTourAndClients().entrySet();
        for (Map.Entry<String, Set<String>> tourMap : toursMap) {
            String tour = tourMap.getKey();
            Set<String> clients = tourMap.getValue();
            for (String client : clients) {
                WebSocketSession session = getSessionsById(client);
                try {
                    session.sendMessage(new TextMessage("Hello client " + client + " " + tour + " was updated"));
                }catch (IOException ex) {
                    sessions.remove(client);
                }
            }
        }
    }
}