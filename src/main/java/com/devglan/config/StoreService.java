package com.devglan.config;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@Scope("singleton")
public class StoreService {
    HashMap<String, List<String>> map;
    {
        map = new HashMap<>();
    }
}
