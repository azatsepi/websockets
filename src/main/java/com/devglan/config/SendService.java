package com.devglan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

//@Component
//public class SendService {
//
//
//    @Autowired
//    SocketHandler socketHandler;
//
//    @Scheduled(fixedRate = 5000)
//    public void sendMessageToClient() throws IOException {
//        Set<Map.Entry<String, Set<String>>> toursMap = socketHandler.getTourAndClients().entrySet();
//        for (Map.Entry<String, Set<String>> tourMap : toursMap) {
//            String tour = tourMap.getKey();
//            Set<String> clients = tourMap.getValue();
//            for (String client : clients) {
//                socketHandler.sendMessage(client, tour);
//                WebSocketSession session = socketHandler.getSessionsById(client);
//                session.sendMessage(new TextMessage("Hello client " + client + " " + tour + " was updated"));
//            }
//        }
//    }
//}
